﻿using NUnit.Framework;
using Qadra.Services.AuthenticationService;
using Qadra.SharedModels;

namespace Qadra.Services.UnitTests.AuthenticationService
{
    public class AuthServiceTests
    {
        private AuthService _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new AuthService();
        }

        [Test]
        public void GetUserRole_ShouldReturnFA_WhenTokenContainsFinancialAdvisor()
        {
            var role = _sut.GetUserRole("financial advisor");
            Assert.That(role, Is.EqualTo(UserRole.FinancialAdvisor));
        }

        [Test]
        public void GetUserRole_ShouldReturnClient_WhenTokenContainsClient()
        {
            var role = _sut.GetUserRole("client");
            Assert.That(role, Is.EqualTo(UserRole.Client));
        }

        [Test]
        [TestCase(null)]
        [TestCase("abskalsd")]
        public void GetUserRole_ShouldReturnNone_WhenTokenIsNullOrInvalidValue(string token)
        {
            var role = _sut.GetUserRole(token);
            Assert.That(role, Is.EqualTo(UserRole.None));
        }
    }
    
}
