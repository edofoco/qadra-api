﻿using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Qadra.Services.UsersService;
using Qadra.SharedModels;

namespace Qadra.Services.UnitTests.UsersService
{
    public class UserServiceTests
    {
        private UserService _sut;
        private Mock<IUsersRepository> _mockUserRepository;

        [SetUp]
        public void Setup()
        {
            _mockUserRepository = new Mock<IUsersRepository>();
            _sut = new UserService(_mockUserRepository.Object);
        }

        [Test]
        public async Task GetUser_ReturnsUser()
        {
            _mockUserRepository.Setup(m => m.GetById(It.IsAny<int>())).ReturnsAsync(new User
            {
                Id = 1,
                Name = "John",
                Lastname = "Do",
                Email = "john.do@qadra.com"
            });

            var user = _sut.GetUser(1);
            Assert.That(user.Id, Is.EqualTo(1));
        }
    }
}
