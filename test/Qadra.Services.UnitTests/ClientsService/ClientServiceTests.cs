﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Qadra.Services.ClientsService;
using Qadra.Services.PortfoliosService;
using Qadra.SharedModels;

namespace Qadra.Services.UnitTests.ClientsService
{
    public class ClientServiceTests
    {
        private ClientService _sut;
        private Mock<IPortfolioService> _mockPortfolioService;

        [SetUp]
        public void Setup()
        {
            _mockPortfolioService = new Mock<IPortfolioService>();
            _sut = new ClientService(_mockPortfolioService.Object);
        }

        [Test]
        public async Task GetPortfolioAssets_ShouldReturnPortfolioAssets()
        {
            _mockPortfolioService.Setup(m => m.GetPortfolioById(It.IsAny<int>())).ReturnsAsync(new Portfolio
            {
                Id = 1,
                ClientId = 1,
                Name = "Portfolio 1",
                RiskLevel = 4
            });

            _mockPortfolioService.Setup(m => m.GetPortfolioAssets(It.IsAny<int>())).ReturnsAsync(new List<Asset>
            {
                new Asset {Id = 1, Name = "ACB"}
            });

            var assets = await _sut.GetPortfolioAssets(1, 1);
            Assert.That(assets.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task GetPortfolioAssets_ShouldReturnNull_WhenPortfolioDoesNotBelongToClient()
        {
            _mockPortfolioService.Setup(m => m.GetPortfolioById(It.IsAny<int>())).ReturnsAsync(new Portfolio
            {
                Id = 1,
                ClientId = 2,
                Name = "Portfolio 1",
                RiskLevel = 4
            });

            _mockPortfolioService.Setup(m => m.GetPortfolioAssets(It.IsAny<int>())).ReturnsAsync(new List<Asset>
            {
                new Asset {Id = 1, Name = "ACB"}
            });

            var assets = await _sut.GetPortfolioAssets(1, 1);
            Assert.IsNull(assets);
        }

    }
}
