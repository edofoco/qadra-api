﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Qadra.Services.PortfoliosService;
using Qadra.SharedModels;

namespace Qadra.Services.UnitTests.PortfolioService
{
    public class PortfolioServiceTests
    {
        private PortfoliosService.PortfolioService _sut;
        private Mock<IPortfolioRepository> _mockPortfolioRepository;

        [SetUp]
        public void Setup()
        {
            _mockPortfolioRepository = new Mock<IPortfolioRepository>();
            _sut = new PortfoliosService.PortfolioService(_mockPortfolioRepository.Object);
        }

        [Test]
        public async Task GetPortfolioById_ShouldReturnPortfolio()
        {
            _mockPortfolioRepository.Setup(m => m.GetPortfolioById(It.IsAny<int>())).ReturnsAsync(new Portfolio
            {
                Id = 1,
                ClientId = 1,
                Name = "Portfolio 1",
                RiskLevel = 4
            });

            var portfolio = await _sut.GetPortfolioById(1);
            Assert.That(portfolio.Id, Is.EqualTo(1));
        }

        [Test]
        public async Task GetAssetsByPortfolioId_ShouldReturnPortfolioAssets()
        {
            _mockPortfolioRepository.Setup(m => m.GetAssetsByPortfolioId(It.IsAny<int>())).ReturnsAsync(new List<Asset>
            {
                new Asset{Id = 1, Name = "ACB"},
                new Asset{Id = 2, Name = "INSY"}
            });

            var assets = await _sut.GetPortfolioAssets(1);
            Assert.That(assets.Count, Is.EqualTo(2));
        }
    }
}
