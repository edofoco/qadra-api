using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Qadra.Services.FinancialAdvisorsService;
using Qadra.Services.FinancialAdvisorsService.Models;
using Qadra.Services.PortfoliosService;
using Qadra.Services.UsersService;
using Qadra.SharedModels;

namespace Qadra.Services.UnitTests.FinancialAdvisorsService
{
    public class FinancialAdvisorTests
    {
        private FinancialAdvisorService _sut;
        private Mock<IFinancialAdvisorRepository> _mockFaRepository;
        private Mock<IUserService> _mockUserService;
        private Mock<IPortfolioService> _mockPortfolioService;

        [SetUp]
        public void Setup()
        {
            _mockFaRepository = new Mock<IFinancialAdvisorRepository>();
            _mockUserService = new Mock<IUserService>();
            _mockPortfolioService = new Mock<IPortfolioService>();

            _sut = new FinancialAdvisorService(_mockFaRepository.Object, _mockUserService.Object, _mockPortfolioService.Object);
        }

        [Test]
        public async Task GetClients_ShouldReturnListOfClients()
        {
            _mockFaRepository.Setup(m => m.GetFinancialAdvisorClients(It.IsAny<int>())).ReturnsAsync(new List<FaClient>
            {
                new FaClient
                {
                    Id = 1,
                    Name = "John",
                    Lastname = "Do",
                    PortfolioId = 1
                }
            });

            var clients = await _sut.GetClients(1);
            Assert.That(clients.Count, Is.EqualTo(1));
            Assert.That(clients[0].Id, Is.EqualTo(1));
        }

        [Test]
        public async Task GetClientById_ShouldReturnClientWithFullUserDetails()
        {
            _mockFaRepository.Setup(m => m.GetFinancialAdvisorClients(It.IsAny<int>())).ReturnsAsync(new List<FaClient>
            {
                new FaClient()
                {
                    Id = 1,
                    Name = "John",
                    Lastname = "Do",
                    PortfolioId = 1
                }
            });

            _mockUserService.Setup(m => m.GetUser(It.IsAny<int>())).ReturnsAsync(new User
            {
                Id = 2,
                Name = "John",
                Lastname = "Do",
                Email = "john.do@qadra.com",
                AddressLine1 = "via del Corso",
                Contact = "+39339999999"
            });

            var client = await _sut.GetClientById(1, 1);
            Assert.That(client.Id, Is.EqualTo(1));
            Assert.That(client.Contact, Is.EqualTo("+39339999999"));
        }

        [Test]
        public async Task GetClientById_ShouldReturnNull_WhenClientDoesNotBelongToFA()
        {
            _mockFaRepository.Setup(m => m.GetFinancialAdvisorClients(It.IsAny<int>())).ReturnsAsync(new List<FaClient>
            {
                new FaClient
                {
                    Id = 2,
                    Name = "John",
                    Lastname = "Do",
                    PortfolioId = 2
                }
            });

            var client = await _sut.GetClientById(1, 1);
            Assert.IsNull(client);
        }

        [Test]
        public async Task GetClientPortfolio_ShouldReturnPortfolio()
        {
            _mockFaRepository.Setup(m => m.GetFinancialAdvisorClients(It.IsAny<int>())).ReturnsAsync(new List<FaClient>
            {
                new FaClient
                {
                    Id = 1,
                    Name = "John",
                    Lastname = "Do",
                    PortfolioId = 1
                }
            });

            _mockPortfolioService.Setup(m => m.GetPortfolioById(1)).ReturnsAsync(new Portfolio
            {
                Id = 1,
                Name ="Portfolio 1",
                RiskLevel = 4
            });
            var client = await _sut.GetClientPortfolio(1, 1);
            Assert.That(client.Id, Is.EqualTo(1));
        }

        [Test]
        public async Task GetClientPortfolio_ShouldReturnNull_WhenClientDoesNotBelongToFA()
        {
            _mockFaRepository.Setup(m => m.GetFinancialAdvisorClients(It.IsAny<int>())).ReturnsAsync(new List<FaClient>
            {
                new FaClient
                {
                    Id = 2,
                    Name = "John",
                    Lastname = "Do",
                    PortfolioId = 2
                }
            });

            var client = await _sut.GetClientPortfolio(1, 1);
            Assert.IsNull(client);
        }

        [Test]
        public async Task GetClientPortfolioAssets_ShouldReturnAssets()
        {
            _mockFaRepository.Setup(m => m.GetFinancialAdvisorClients(It.IsAny<int>())).ReturnsAsync(new List<FaClient>
            {
                new FaClient
                {
                    Id = 1,
                    Name = "John",
                    Lastname = "Do",
                    PortfolioId = 1
                }
            });

            _mockPortfolioService.Setup(m => m.GetPortfolioAssets(It.IsAny<int>())).ReturnsAsync(new List<Asset>
            {
                new Asset
                {
                    Id = 1,
                    Name = "ACB"
                }
            });

            var assets = await _sut.GetClientPortfolioAssets(1, 1);
            Assert.That(assets.Count, Is.EqualTo(1));
        }
    }
}