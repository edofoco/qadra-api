using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Qadra.Api.Controllers;
using Qadra.Services.AuthenticationService;
using Qadra.Services.FinancialAdvisorsService;
using Qadra.Services.FinancialAdvisorsService.Models;
using Qadra.SharedModels;

namespace Qadra.Api.UnitTests.Controllers
{
    public class ClientsControllerTests
    {
        ClientsController _sut;
        Mock<IFinancialAdvisorService> _mockFaService;
        private Mock<IAuthService> _mockAuthService;

        [SetUp]
        public void Setup()
        {
            _mockFaService = new Mock<IFinancialAdvisorService>();
            _mockAuthService = new Mock<IAuthService>();

            _mockAuthService.Setup(m => m.GetUserRole(It.IsAny<string>())).Returns(UserRole.FinancialAdvisor);
            _sut = new ClientsController(_mockFaService.Object, _mockAuthService.Object);
        }

        [Test]
        public async Task GetClients_ShouldReturn200WithClientsList()
        {
            _sut.ControllerContext.HttpContext = new DefaultHttpContext();
            _sut.ControllerContext.HttpContext.Request.Headers["Authorization"] = "hello";

            _mockFaService.Setup(m => m.GetClients(It.IsAny<int>())).ReturnsAsync(new List<FaClient>
            {
                new FaClient
                {
                    Id = 1,
                    Name = "John",
                    Lastname = "Do"
                }
            });

            var rawResponse = await _sut.GetClients();
            var response = (OkObjectResult)rawResponse;
            var value = (List<FaClient>)response.Value;

            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(value.Count, Is.EqualTo(1));
            Assert.That(value[0].Id, Is.EqualTo(1));
        }


        [Test]
        public async Task GetClient_ShouldReturn200WithClientDetails()
        {
            _sut.ControllerContext.HttpContext = new DefaultHttpContext();
            _sut.ControllerContext.HttpContext.Request.Headers["Authorization"] = "hello";

            _mockFaService.Setup(m => m.GetClientById(It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync(new FaClient
            {
                Id = 1,
                Name = "John",
                Lastname = "Do"
            });

            var rawResponse = await _sut.GetClient(1);
            var response = (OkObjectResult)rawResponse;
            var value = (User)response.Value;

            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(value.Id, Is.EqualTo(1));
        }

        [Test]
        public async Task GetClient_ShouldReturn404_WhenClientNotFound()
        {
            _sut.ControllerContext.HttpContext = new DefaultHttpContext();
            _sut.ControllerContext.HttpContext.Request.Headers["Authorization"] = "hello";

            _mockFaService.Setup(m => m.GetClientById(It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync((FaClient)null);

            var rawResponse = await _sut.GetClient(1);
            var response = (NotFoundResult)rawResponse;

            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status404NotFound));
        }
    }
}