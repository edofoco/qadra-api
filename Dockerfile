FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src
COPY ["src/Qadra.Api/Qadra.Api.csproj", "src/Qadra.Api/"]
COPY ["src/Qadra.SharedModels/Qadra.SharedModels.csproj", "src/Qadra.SharedModels/"]
COPY ["src/Qadra.Services/Qadra.Services.csproj", "src/Qadra.Services/"]
RUN dotnet restore "src/Qadra.Api/Qadra.Api.csproj"
COPY . .
WORKDIR "/src/src/Qadra.Api"
RUN dotnet build "Qadra.Api.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Qadra.Api.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Qadra.Api.dll"]