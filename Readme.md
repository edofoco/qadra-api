# Qadra API

### Enpoints
- GET /clients
- GET /clients/{id}
- GET /clients/{id}/portfolios
- GET /portfolios/{id}
 
### Infrastructure
- Deployed using CloudFormation
- Docker
- AWS ECS + ECR

