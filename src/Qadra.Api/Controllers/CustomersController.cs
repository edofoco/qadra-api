﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Qadra.Services.AuthenticationService;
using Qadra.Services.FinancialAdvisorsService;
using Qadra.SharedModels;

namespace Qadra.Api.Controllers
{
    [Route("/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly int tmpFinancialAdvisorid = 2; //Should come from bearer on every request
        private readonly IFinancialAdvisorService _faService;
        private readonly IAuthService _authService;

        public CustomersController(IFinancialAdvisorService faService, IAuthService authService)
        {
            _faService = faService;
            _authService = authService;
        }

        [HttpGet]
        public async Task<IActionResult> GetClients()
        {
            if (!UserIsFinancialAdvisor(Request.Headers["Authorization"]))
                return Unauthorized();

            return Ok(await _faService.GetClients(tmpFinancialAdvisorid));
        }

        [HttpGet]
        [Route("/customers/{id}")]
        public async Task<ActionResult> GetClient(int id)
        {
            if (!UserIsFinancialAdvisor(Request.Headers["Authorization"]))
                return Unauthorized();

            var client = await _faService.GetClientById(tmpFinancialAdvisorid, id);
            if (client == null)
            {
                return new NotFoundResult();
            }

            return Ok(client);
        }

        [HttpGet]
        [Route("/customers/{id}/portfolios")]
        public async Task<ActionResult> GetClientPortfolio(int id)
        {
            if (!UserIsFinancialAdvisor(Request.Headers["Authorization"]))
                return Unauthorized();

            var portfolio = await _faService.GetClientPortfolio(tmpFinancialAdvisorid, id);
            if (portfolio == null)
            {
                return new NotFoundResult();
            }

            return Ok(portfolio);
        }

        private bool UserIsFinancialAdvisor(string authToken)
        {
            //This should be changed to a proper Authorization Attribute
            return UserRole.FinancialAdvisor == _authService.GetUserRole(authToken);
        }
    }
}
