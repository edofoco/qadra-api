﻿using Microsoft.AspNetCore.Mvc;

namespace Qadra.Api.Controllers
{
    [ApiController]
    public class HealthcheckController : ControllerBase
    {
        [Route("/ping")]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok();
        }
    }
}
