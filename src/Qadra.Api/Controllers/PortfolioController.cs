﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Qadra.Services.AuthenticationService;
using Qadra.Services.ClientsService;
using Qadra.Services.FinancialAdvisorsService;
using Qadra.SharedModels;

namespace Qadra.Api.Controllers
{
    [Route("/portfolios")]
    [ApiController]
    public class PortfolioController : ControllerBase
    {
        private readonly int tmpFinancialAdvisorId = 2; //Should be taken from bearer token
        private readonly int tmpClientId = 1; //Should be taken from bearer token

        private readonly IFinancialAdvisorService _financialAdvisorService;
        private readonly IClientService _clientService;
        private readonly IAuthService _authService;

        public PortfolioController(IFinancialAdvisorService financialAdvisorService, IAuthService authService, IClientService clientService)
        {
            _financialAdvisorService = financialAdvisorService;
            _authService = authService;
            _clientService = clientService;
        }

        [Route("/portfolios/{id}/assets")]
        [HttpGet]
        public async Task<IActionResult> GetPortfolioAssets(int id)
        {
            var userRole = _authService.GetUserRole(Request.Headers["Authorization"]);
            if (userRole == UserRole.None)
                return Unauthorized();

            List<Asset> assets = null;
            switch (userRole)
            {
                case UserRole.FinancialAdvisor:
                    assets = await _financialAdvisorService.GetClientPortfolioAssets(tmpFinancialAdvisorId, id);
                    break;
                case UserRole.Client:
                    assets = await _clientService.GetPortfolioAssets(tmpClientId, id);
                    break;
            }

            if (assets == null)
            {
                return new NotFoundResult();
            }
            
            return Ok(assets);
        }
        
    }
}
