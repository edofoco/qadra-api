﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Qadra.Api.Filters;
using Qadra.Services.AuthenticationService;
using Qadra.Services.ClientsService;
using Qadra.Services.FinancialAdvisorsService;
using Qadra.Services.PortfoliosService;
using Qadra.Services.UsersService;
using Swashbuckle.AspNetCore.Swagger;

namespace Qadra.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Qadra API", Version = "v1" });
                c.OperationFilter<SwaggerAuthFilter>();
            });

            services.AddSingleton<IFinancialAdvisorRepository, FinancialAdvisorRepository>();
            services.AddSingleton<IFinancialAdvisorService, FinancialAdvisorService>();
            services.AddSingleton<IUsersRepository, UsersRepository>();
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<IPortfolioService, PortfolioService>();
            services.AddSingleton<IPortfolioRepository, PortfolioRepository>();
            services.AddSingleton<IAuthService, AuthService>();
            services.AddSingleton<IClientService, ClientService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseMvc();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Qadra API V1");
            });

        }
    }
}
