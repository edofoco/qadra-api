﻿using System.Collections.Generic;

namespace Qadra.SharedModels
{
    public class Portfolio
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Name { get; set; }
        public int RiskLevel { get; set; }
    }
}
