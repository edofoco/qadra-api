﻿namespace Qadra.SharedModels
{
    public class Asset
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
