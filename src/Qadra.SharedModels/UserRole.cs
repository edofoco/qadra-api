﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qadra.SharedModels
{
    public enum UserRole
    {
        FinancialAdvisor,
        Client,
        None
    }
}
