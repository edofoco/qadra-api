﻿using System;

namespace Qadra.SharedModels
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string AddressLine1 { get; set; }
    }
}
