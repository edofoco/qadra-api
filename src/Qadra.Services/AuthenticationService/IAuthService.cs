﻿using Qadra.SharedModels;

namespace Qadra.Services.AuthenticationService
{
    public interface IAuthService
    {
        UserRole GetUserRole(string authToken);
    }
}
