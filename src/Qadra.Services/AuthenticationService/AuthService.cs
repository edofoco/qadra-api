﻿using System;
using Qadra.SharedModels;

namespace Qadra.Services.AuthenticationService
{
    public class AuthService : IAuthService
    {
        public UserRole GetUserRole(string authToken)
        {
            if (authToken != null && authToken.Contains("financial advisor", StringComparison.CurrentCultureIgnoreCase))
            {
                return UserRole.FinancialAdvisor;
            }

            if (authToken != null && authToken.Contains("client", StringComparison.CurrentCultureIgnoreCase))
            {
                return UserRole.Client;
            }

            return UserRole.None;

        }
    }
}
