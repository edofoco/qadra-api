﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Qadra.Services.PortfoliosService;
using Qadra.SharedModels;

namespace Qadra.Services.ClientsService
{
    public class ClientService : IClientService
    {
        private readonly IPortfolioService _portfolioService;

        public ClientService(IPortfolioService portfolioService)
        {
            _portfolioService = portfolioService;
        }

        public async Task<List<Asset>> GetPortfolioAssets(int clientId, int portfolioId)
        {
            var portfolio = await _portfolioService.GetPortfolioById(portfolioId);
            if (portfolio.ClientId != clientId) return null;

            return await _portfolioService.GetPortfolioAssets(portfolioId);
        }
    }
}
