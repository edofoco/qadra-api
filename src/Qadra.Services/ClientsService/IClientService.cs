﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Qadra.SharedModels;

namespace Qadra.Services.ClientsService
{
    public interface IClientService
    {
        Task<List<Asset>> GetPortfolioAssets(int clientId, int portfolioId);
    }
}
