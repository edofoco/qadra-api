﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Qadra.Services.FinancialAdvisorsService.Models;
using Qadra.SharedModels;

namespace Qadra.Services.FinancialAdvisorsService
{
    public interface IFinancialAdvisorService
    {
        Task<List<FaClient>> GetClients(int financialAdvisorId);
        Task<FaClient> GetClientById(int financialAdvisorId, int id);
        Task<Portfolio> GetClientPortfolio(int financialAdvisorId, int clientId);
        Task<List<Asset>> GetClientPortfolioAssets(int financialAdvisorId, int portfolioId);
    }
}
