﻿using Qadra.SharedModels;

namespace Qadra.Services.FinancialAdvisorsService.Models
{
    public class FaClient : User
    {
        public int PortfolioId { get; set; }

        public void FillAdditionalUserDetails(User user)
        {
            Email = user.Email;
            Contact = user.Contact;
            AddressLine1 = user.AddressLine1;
        }
    }
}
