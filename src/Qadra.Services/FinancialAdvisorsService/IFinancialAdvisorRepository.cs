﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Qadra.Services.FinancialAdvisorsService.Models;

namespace Qadra.Services.FinancialAdvisorsService
{
    public interface IFinancialAdvisorRepository
    {
        Task<List<FaClient>> GetFinancialAdvisorClients(int financialAdvisorId);
    }
}
