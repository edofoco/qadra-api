﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Qadra.Services.FinancialAdvisorsService.Models;
using Qadra.Services.PortfoliosService;
using Qadra.Services.UsersService;
using Qadra.SharedModels;

namespace Qadra.Services.FinancialAdvisorsService
{
    public class FinancialAdvisorService : IFinancialAdvisorService
    {
        private readonly IFinancialAdvisorRepository _faRepository;
        private readonly IUserService _userService;
        private readonly IPortfolioService _portfolioService;

        public FinancialAdvisorService(IFinancialAdvisorRepository faRepository, IUserService userService, IPortfolioService portfolioService)
        {
            _faRepository = faRepository;
            _userService = userService;
            _portfolioService = portfolioService;
        }
            
        public async Task<List<FaClient>> GetClients(int financialAdvisorId)
        {
            var clients = await _faRepository.GetFinancialAdvisorClients(financialAdvisorId);
            return new List<FaClient>(clients);
        }

        public async Task<FaClient> GetClientById(int financialAdvisorId, int clientId)
        {
            var client = await GetClient(financialAdvisorId, clientId);
            if (client == null) return null;

            var userDetails = await _userService.GetUser(client.Id);
            client.FillAdditionalUserDetails(userDetails);
            return client;
        }

        public async Task<Portfolio> GetClientPortfolio(int financialAdvisorId, int clientId)
        {
            var client = await GetClient(financialAdvisorId, clientId);
            if (client == null) return null;

            return await _portfolioService.GetPortfolioById(client.PortfolioId);
        }

        public async Task<List<Asset>> GetClientPortfolioAssets(int financialAdvisorId, int portfolioId)
        {
            if (!await HasAccessToPortfolio(financialAdvisorId, portfolioId)) return null;
            return await _portfolioService.GetPortfolioAssets(portfolioId);
        }

        private async Task<FaClient> GetClient(int financialAdvisorId, int clientId)
        {
            var clients = await _faRepository.GetFinancialAdvisorClients(financialAdvisorId);
            return clients.FirstOrDefault(c => c.Id == clientId);
        }

        private async Task<bool> HasAccessToPortfolio(int financialAdvisorId, int portfolioId)
        {
            var clients = await _faRepository.GetFinancialAdvisorClients(financialAdvisorId);
            var client = clients.FirstOrDefault(c => c.PortfolioId == portfolioId);
            return client != null;
        }
    }
}
