﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Qadra.Services.FinancialAdvisorsService.Models;
using Qadra.SharedModels;

namespace Qadra.Services.FinancialAdvisorsService
{
    public class FinancialAdvisorRepository : IFinancialAdvisorRepository
    {
        private readonly Dictionary<int, List<FaClient>> _tmpFaDb;

        public FinancialAdvisorRepository()
        {
            var faClients = new List<FaClient>
            {
                new FaClient
                {
                    Id = 1,
                    Name = "James",
                    Lastname = "Franco",
                    PortfolioId = 1
                },
                new FaClient
                {
                    Id = 2,
                    Name = "Hulk",
                    Lastname = "Hogan",
                    PortfolioId = 2
                }
            };

            _tmpFaDb = new Dictionary<int, List<FaClient>>
            {
                {2, faClients}
            };
        }

        public async Task<List<FaClient>> GetFinancialAdvisorClients(int financialAdvisorId)
        {
            return !_tmpFaDb.ContainsKey(financialAdvisorId) ? null : _tmpFaDb[financialAdvisorId];
        }
    }
}
