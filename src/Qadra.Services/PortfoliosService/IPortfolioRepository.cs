﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Qadra.SharedModels;

namespace Qadra.Services.PortfoliosService
{
    public interface IPortfolioRepository
    {
        Task<Portfolio> GetPortfolioById(int id);
        Task<List<Asset>> GetAssetsByPortfolioId(int portfolioId);
    }
}
