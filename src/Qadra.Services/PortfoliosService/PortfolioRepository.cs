﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Qadra.SharedModels;

namespace Qadra.Services.PortfoliosService
{
    public class PortfolioRepository : IPortfolioRepository
    {
        private readonly Dictionary<int, Portfolio> _tmpPortfolioTable; //int is portfolioId
        private readonly Dictionary<int, List<Asset>> _tmpAssetsTable; //int is portfolioId
    
        public PortfolioRepository()
        {
            _tmpPortfolioTable = new Dictionary<int, Portfolio>
                {
                    {1,  new Portfolio {Id = 1, ClientId = 1, Name="Portfolio 1", RiskLevel = 4 }}
                };
       
            _tmpAssetsTable = new Dictionary<int, List<Asset>>
            {   
                {1, new List<Asset>{ new Asset {Id = 1, Name = "ACB"}, new Asset {Id=2, Name = "CRON"}}}
            };
        }

        public async Task<Portfolio> GetPortfolioById(int id)
        {
            return _tmpPortfolioTable.ContainsKey(id) ? _tmpPortfolioTable[id] : null;
        }

        public async Task<List<Asset>> GetAssetsByPortfolioId(int portfolioId)
        {
            var assets = new List<Asset>();
            return _tmpAssetsTable.ContainsKey(portfolioId) ? _tmpAssetsTable[portfolioId] : assets;
        }
    }
}
