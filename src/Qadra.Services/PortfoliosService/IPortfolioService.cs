﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Qadra.SharedModels;

namespace Qadra.Services.PortfoliosService
{
    public interface IPortfolioService
    {
        Task<Portfolio> GetPortfolioById(int id);
        Task<List<Asset>> GetPortfolioAssets(int portfolioId);
    }
}
