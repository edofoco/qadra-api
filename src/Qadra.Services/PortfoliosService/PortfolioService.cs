﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Qadra.SharedModels;

namespace Qadra.Services.PortfoliosService
{
    public class PortfolioService : IPortfolioService
    {
        private readonly IPortfolioRepository _portfolioRepository;

        public PortfolioService(IPortfolioRepository portfolioRepository)
        {
            _portfolioRepository = portfolioRepository;
        }

        public async Task<Portfolio> GetPortfolioById(int id)
        {
            return await _portfolioRepository.GetPortfolioById(id);
        }

        public async Task<List<Asset>> GetPortfolioAssets(int portfolioId)
        {
            return await _portfolioRepository.GetAssetsByPortfolioId(portfolioId);
        }
    }
}
