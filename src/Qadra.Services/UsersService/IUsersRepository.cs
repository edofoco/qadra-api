﻿using System.Threading.Tasks;
using Qadra.SharedModels;

namespace Qadra.Services.UsersService
{
    public interface IUsersRepository
    {
        Task<User> GetById(int id);
    }
}
