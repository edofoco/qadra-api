﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using Qadra.SharedModels;

namespace Qadra.Services.UsersService
{
    public class UsersRepository : IUsersRepository
    {
        private readonly List<User> _tmpUsers;
       
        public UsersRepository()
        {
            _tmpUsers = new List<User>
            {
                new User
                {
                    Id = 1,
                    Name = "James",
                    Lastname = "Franco",
                    Email = "james.franco@qadra.com",
                    Contact = "+39234999999",
                    AddressLine1 = "Via del Corso 21"
                },
                new User
                {
                    Id = 2,
                    Name = "Hulk",
                    Lastname = "Hogan",
                    Email = "hulk.hogan@qadra.com",
                    Contact = "392348888888",
                    AddressLine1 = "P.za di Spagna"
                }
            };
        }

        public async Task<User> GetById(int id)
        {
            return _tmpUsers.FirstOrDefault(u => u.Id == id);
        }
    }
}
