﻿using System.Threading.Tasks;
using Qadra.SharedModels;

namespace Qadra.Services.UsersService
{
    public class UserService : IUserService
    {
        private readonly IUsersRepository _usersRepository;

        public UserService(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public async Task<User> GetUser(int id)
        {
            return await _usersRepository.GetById(id);
        }
    }
}
