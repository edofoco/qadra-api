﻿using System.Threading.Tasks;
using Qadra.SharedModels;

namespace Qadra.Services.UsersService
{
    public interface IUserService
    {
        Task<User> GetUser(int id);
    }
}
